// /@ts-check
/**
 * @type {import('eslint').ESLint.ConfigData}
 */
const config = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: [
    'import',
    'react',
    '@typescript-eslint'
  ],
  rules: {
    '@typescript-eslint/no-explicit-any': [2],
  },
  ignorePatterns: [ '.eslintrc.js', 'asserts/**' ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['ts']
      },
      typescript: {
        alwaysTryTypes: true
      }
    },
    react: {
      version: 'detect'
    }
  }
}

module.exports = config
