// @ts-check
 

const options = {
    define: {
        "process.env.NODE_ENV": '"dev"',
    },
}
/**
 * @type {import('f2e-server').F2EConfig}
 */
const config = {
    livereload: true,
    build: false,
    gzip: true,
    useLess: true,
    renderHeaders: function (headers) {
        return {
            ...headers,
            'Access-Control-Allow-Origin': '*',
        }
    },
    try_files: 'index.html',
    buildFilter: (pathname) => /^(\.esbuild\/?(external|$)|css\/?(bundle|$)|favicon|index|src\/?(index|$)|$)/.test(pathname),
    watchFilter: (pathname) => /^(css|favicon|index|src|$)/.test(pathname),
    middlewares: [
        // 对应文本支持简单ejs语法的服务端渲染，通常用来给资源链接加时间戳
        { middleware: 'template', test: /\.html?/ },
        // 使用 esbuild 编译构建前端资源
        {
            middleware: 'esbuild',
            watches: [/\.(tsx?|less)$/],
            options,
        },
    ],
};
module.exports = config;
