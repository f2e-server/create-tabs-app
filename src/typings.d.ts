import { ColumnType } from 'antd/lib/table';
import InternalForm from 'antd/lib/form/Form'
import Form, { FormItemProps } from 'antd/lib/form'
import React, { PropsWithChildren } from 'react';

// API 类型
declare interface FetchResult<T> {
    success?: boolean;
    code?: boolean;
    status?: boolean;
    data?: T
}
declare interface FetchAPI<F, T> {
    (body?: F): Promise<FetchResult<T>>
}
declare interface PageParam {
    page?: number;
    size?: number;
}
declare interface Pager<T> extends PageParam {
    total?: number;
    data?: T[];
}

// 类型计算函数
declare type RouterParamKeys<T extends string, S extends string = '/', M extends string = ':'> = T extends `${infer A}${S}${infer B}` ? (
    A extends `${M}${infer R}` ? (R | RouterParamKeys<B, S, M>) : RouterParamKeys<B, S, M>
) : (T extends `${M}${infer R}` ? R : never)

declare type RouterParam<T extends string> = RouterParamKeys<T> extends never ? never : Required<Record<RouterParamKeys<T>, string>>
/** 给组件增加children类型, 解决React18 和 antd4的不兼容问题 */
declare type Childrenfy<T extends React.ComponentType> = T extends React.ComponentType<infer P> ? React.ComponentType<PropsWithChildren<P>> : T

/** antd 类型修改 */
declare interface _ColumnType<T> extends Omit<ColumnType<T>, 'dataIndex'> {
    dataIndex?: keyof T | '_index',
}
declare type ColumnsType<T> = _ColumnType<T>[]

type _InternalFormType<T> = typeof InternalForm<T>;
type _FormInterface = typeof Form
type _FormItem<T> = {(props: Omit<FormItemProps<T>, 'name'> & {
  name: keyof T | (keyof T)[]
}): React.ReactElement}
declare interface FormType<T> extends Omit<_FormInterface, "Item">, _InternalFormType<T> {
  Item: _FormItem<T>
}

// 全局moudle
declare module "*.svg" {
    const content: string;
    export default content;
}

declare module "*.png" {
    const content: string;
    export default content;
}

declare module "*.jpg" {
    const content: string;
    export default content;
}

declare module "*.json" {
    const content: object;
    export default content;
}

declare module "*.txt" {
    const content: string;
    export default content;
}

declare module "*.less" {
    const content: Record<string, string>;
    export default content;
}

declare module '*.mp3';