import * as antd from 'antd'
import { Childrenfy } from '../typings'

export const Dropdown = antd.Dropdown as Childrenfy<typeof antd.Dropdown>
export const Modal = antd.Modal as Childrenfy<typeof antd.Modal>
