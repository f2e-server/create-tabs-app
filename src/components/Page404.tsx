import { Button, Result } from 'antd';
import * as React from 'react'
import { closeTab, history } from '../store';

const Page404 = () => (
    <Result
      status="404"
      title="404"
      style={{ background: '#fff' }}
      subTitle={`你访问的页面不存在 ${history.location.pathname}`}
      extra={<Button type="primary" onClick={() => closeTab()}>关闭页面</Button>}
    />
  );
  
  export default Page404