import { Menu } from 'antd'
import * as React from 'react'
import { MenuKey, menu_list } from '../menu'
import { connect, getState, openTab } from '../store'

interface Props {
    pathname: string
    collapsed: boolean
}
const _SiderMenu = (props: Props) => {
    const { pathname, collapsed } = props
    return <Menu
        mode="inline"
        defaultOpenKeys={collapsed ? [] : menu_list.map(m => m.key)}
        selectedKeys={[pathname]}
        onClick={({key}) => openTab(key as MenuKey, null)}
    >
        {menu_list.map(m => m.children?.length > 0 ? <Menu.SubMenu title={m.title} key={m.key} icon={m.icon}>
            {m.children.map(m1 => <Menu.Item key={m1.key} icon={m1.icon}>{m1.title}</Menu.Item>)}
        </Menu.SubMenu> : <Menu.Item key={m.key} icon={m.icon}>{m.title}</Menu.Item>)}
    </Menu>
}

const SiderMenu = connect(() => {
    const { pathname, collapsed } = getState()
    return { pathname, collapsed }
})(_SiderMenu)

export default SiderMenu