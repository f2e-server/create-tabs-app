import { Button, Menu, Result, Tabs, TabsProps } from 'antd'
import { CloseCircleOutlined } from '@ant-design/icons'
import * as React from 'react'
import { menu_map } from '../menu'
import { closeTab, closeTabAll, closeTabOthers, connect, getState, history, IStoreState } from '../store'
import styles from './index.less';
import { Dropdown, Modal } from '../components'

const Placeholder = () => {
    return <div style={{ padding: 16 }}>
        <Result status="404" style={{
            padding: 120,
        }} subTitle="暂时还没有打开页面, 点击左侧导航打开" />
    </div>
}

const Extra = () => {
    const [visible, setVisible] = React.useState(false)

    const onOk = () => {
        setVisible(false)
        closeTabAll()
    }
    const onCancel = () => {
        setVisible(false)
    }
    const openModal = () => {
        setVisible(true)
    }
    return <>
        <Dropdown overlayStyle={{ width: 120 }} overlay={<Menu>
            <Menu.Item key="1" onClick={() => closeTab()}>关闭当前标签</Menu.Item>
            <Menu.Item key="2" onClick={() => closeTabOthers()}>关闭其他标签</Menu.Item>
            <Menu.Item key="3" onClick={openModal}>关闭所有标签</Menu.Item>
        </Menu>} trigger={['hover']} >
            <Button type="text">
                <CloseCircleOutlined />
            </Button>
        </Dropdown>
        <Modal title="警告" onOk={onOk} onCancel={onCancel} visible={visible}>
            确定关闭所有标签
        </Modal>
    </>
}

const _TopTabs = (props: Pick<IStoreState, 'tabs' | 'pathname'>) => {
    const { tabs, pathname } = props

    if (!(tabs.length > 0 && /\w/.test(pathname))) {
        return <Placeholder />
    }

    const onTabClick: TabsProps['onTabClick'] = (key) => {
        history.push(key)
    }
    const onEdit: TabsProps['onEdit'] = (e, action) => {
        switch (action) {
            case 'remove':
                closeTab(e as string)
                break
        }
    }

    return <Tabs
        type="editable-card"
        hideAdd
        destroyInactiveTabPane
        size="small"
        className={styles.top_tabs}
        tabBarGutter={8}
        tabBarExtraContent={<Extra />}
        activeKey={pathname}
        onTabClick={onTabClick}
        onEdit={onEdit}>
        {tabs.map(t => {
            const item = menu_map.get(t.menuKey)
            return item && item.component && <Tabs.TabPane key={t.pathname} tabKey={t.pathname} tab={t.title || item.title}>
                <item.component params={t.params} />
            </Tabs.TabPane>
        })}
    </Tabs>
}

const TopTabs = connect(() => {
    const { tabs, pathname } = getState()
    return { tabs, pathname }
})(_TopTabs)

export default TopTabs