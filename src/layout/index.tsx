import * as React from 'react'
import { Layout } from 'antd'
import { getState, connect, dispatch, IStoreState } from '../store'
import SiderMenu from './menu'
import TopTabs from './tabs'

const { Sider } = Layout

const _Index = (props: Pick<IStoreState, 'collapsed' | 'top' | 'rect'>) => {
    const { collapsed, top, rect } = props
    return <Layout style={{ height: rect.height - top }}>
        <Sider theme="light" collapsible collapsed={collapsed} collapsedWidth={56} onCollapse={collapsed => dispatch(state => ({ ...state, collapsed }))}>
            <SiderMenu />
        </Sider>
        <Layout>
            <TopTabs />
        </Layout>
    </Layout>
}

const Index = connect(() => {
    const { collapsed, top, rect } = getState()
    return { collapsed, top, rect }
})(_Index)

export default Index