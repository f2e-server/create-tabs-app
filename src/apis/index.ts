import { FetchAPI, PageParam, Pager } from '../typings'
import { Fetch } from './Fetch'

export interface UserQuery {
    name?: string;
    ageRange?: [min?: number, max?: number];
}
export interface UserInfo {
    id: number;
    name: string;
    gender: '1' | '2';
    age: number;
}
export const api_common: {
    __list: FetchAPI<UserQuery, Pager<UserInfo>>;
    list: FetchAPI<UserQuery & PageParam, Pager<UserInfo>>;
    getUser: FetchAPI<number, UserInfo>;
} = {
    // 真实接口调用
    __list: (body = {}) => Fetch(`/api/test`, body),
    list: async (body) => {
        const { page = 1, size = 10, ageRange = [], name } = body
        const result = mockUsers.filter(u => {
            const [ min = 0, max = Number.MAX_SAFE_INTEGER ] = ageRange
            return (!name || u.name.includes(name)) && u.age >= min && u.age <= max
        })
        const begin = (page - 1) * size
        const end = begin + size
        return {
            success: true,
            data: {
                total: result.length,
                data: result.slice(begin, end)
            }
        }
    },
    getUser: async (id) => {
        const user = mockUsers.find(u => u.id === id)
        return {
            success: true,
            data: user
        }
    },
}

const mockUsers: UserInfo[] = [
    { id: 1, name: '赵四儿', gender: '1', age: 40 },
    { id: 2, name: '钱三一', gender: '1', age: 20 },
    { id: 3, name: '孙二娘', gender: '2', age: 37 },
    { id: 4, name: '李四四', gender: '2', age: 30 },
    { id: 5, name: '陈千千', gender: '2', age: 20 },
    { id: 6, name: '武三四', gender: '1', age: 50 },
    { id: 7, name: '朱七七', gender: '2', age: 20 },
    { id: 8, name: '朱八八', gender: '1', age: 50 },
    { id: 9, name: '沈万三', gender: '1', age: 36 },
    
    { id: 11, name: '赵四儿(1)', gender: '1', age: 40 },
    { id: 12, name: '钱三一(1)', gender: '1', age: 20 },
    { id: 13, name: '孙二娘(1)', gender: '2', age: 37 },
    { id: 14, name: '李四四(1)', gender: '2', age: 30 },
    { id: 15, name: '陈千千(1)', gender: '2', age: 20 },
    { id: 16, name: '武三四(1)', gender: '1', age: 50 },
    { id: 17, name: '朱七七(1)', gender: '2', age: 20 },
    { id: 18, name: '朱八八(1)', gender: '1', age: 50 },
    { id: 19, name: '沈万三(1)', gender: '1', age: 36 },

    { id: 21, name: '赵四儿(2)', gender: '1', age: 40 },
    { id: 22, name: '钱三一(2)', gender: '1', age: 20 },
    { id: 23, name: '孙二娘(2)', gender: '2', age: 37 },
    { id: 24, name: '李四四(2)', gender: '2', age: 30 },
    { id: 25, name: '陈千千(2)', gender: '2', age: 20 },
    { id: 26, name: '武三四(2)', gender: '1', age: 50 },
    { id: 27, name: '朱七七(2)', gender: '2', age: 20 },
    { id: 28, name: '朱八八(2)', gender: '1', age: 50 },
    { id: 29, name: '沈万三(2)', gender: '1', age: 36 },

    { id: 31, name: '赵四儿(3)', gender: '1', age: 40 },
    { id: 32, name: '钱三一(3)', gender: '1', age: 20 },
    { id: 33, name: '孙二娘(3)', gender: '2', age: 37 },
    { id: 34, name: '李四四(3)', gender: '2', age: 30 },
    { id: 35, name: '陈千千(3)', gender: '2', age: 20 },
    { id: 36, name: '武三四(3)', gender: '1', age: 50 },
    { id: 37, name: '朱七七(3)', gender: '2', age: 20 },
    { id: 38, name: '朱八八(3)', gender: '1', age: 50 },
    { id: 39, name: '沈万三(3)', gender: '1', age: 36 },
];

