export const pathname2params = (template: string) => {
    const keys = ['']
    const reg = new RegExp(`^${
        template.replace(/:([^\\/]+)/g, (_, n) => {
            keys.push(n)
            return '([^\\\\/]+)'
        })
    }$`)

    return (pathname: string) => {
        const matches = pathname.match(reg)
        const params: Record<string, string> = {}
        if (matches) {
            for (let i = 1; i < matches.length; i++) {
                const ele = matches[i];
                params[keys[i]] = ele
            }
            return params
        }
    }
}