import * as React from 'react'
import { HomeOutlined } from '@ant-design/icons'
import UserList from './routes/user'
import UserDetail from './routes/user/detail'
import WebTask from './routes/task'
import WebGoods from './routes/goods'
import Page404 from './components/Page404'

export enum MenuKey {
    /** 基础目录 */ BASE = "/base",
    /** 用户列表 */ USERS = "/users",
    /** 用户详情 */ USERS_DETAIL = "/users/:id",
    /** 任务详情 */ TASKS = "/tasks",
    /** 商品详情 */ GOODS = "/goods",

    /** 异常页面 */ PAGE_404 = 'page_404',
}

export interface MenuInfo {
    key: MenuKey;
    title?: string;
    icon?: React.ReactNode;
    component?: React.ComponentType<{ params?: Record<string, string> }>;
    parent_key?: MenuKey;
    hidden?: boolean;
    children?: MenuInfo[]
}
export const route_list: MenuInfo[] = [
    { key: MenuKey.BASE, title: '基础目录', icon: <HomeOutlined /> },
    { key: MenuKey.USERS, title: '用户列表', component: UserList, parent_key: MenuKey.BASE },
    { key: MenuKey.USERS_DETAIL, title: '用户详情', component: UserDetail, parent_key: MenuKey.BASE, hidden: true },
    { key: MenuKey.TASKS, title: '任务详情', component: WebTask, parent_key: MenuKey.BASE },
    { key: MenuKey.GOODS, title: '商品详情', component: WebGoods, parent_key: MenuKey.BASE },

    { key: MenuKey.PAGE_404, title: '页面不存在', component: Page404, hidden: true },
];
export const menu_map = new Map(route_list.map(menu => [menu.key, menu]))
export const menu_list = route_list.filter(menu => {
    if (!menu.hidden && menu.parent_key) {
        const parent = menu_map.get(menu.parent_key)
        parent.children = (parent.children || []).concat(menu)
    }
    return !menu.hidden && !menu.parent_key
})