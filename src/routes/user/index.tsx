import { Button, Table } from 'antd'
import * as React from 'react'
import { api_common, UserInfo } from '../../apis'
import { MenuKey } from '../../menu'
import { openTab } from '../../store'
import { ColumnsType, PageParam, Pager } from '../../typings'

const columns: ColumnsType<UserInfo> = [
    {
        dataIndex: '_index',
        title: '#',
        render: (v, r, index) => ('00' + ++index).slice(-2),
    },
    {
        dataIndex: 'name',
        title: '姓名',
    },
    {
        dataIndex: 'gender',
        title: '性别',
        render: t => ['[未知]', '男', '女'][t | 0],
    },
    {
        dataIndex: 'age',
        title: '年龄',
    },
    {
        dataIndex: 'id',
        title: '操作',
        render: (id) => <Button size="small" type="link" onClick={() => openTab(MenuKey.USERS_DETAIL, { id })}>查看</Button>
    }
]

const UserList = () => {
    const [data, setData] = React.useState<Pager<UserInfo>>()
    const [pager, setPager] = React.useState<PageParam>({ page: 1, size: 10})

    const doSearch = async () => {
        api_common.list({
            ...pager,
        }).then(res => setData(res.data))
    }

    React.useEffect(function () {
        doSearch()
    }, [pager])

    return (
        <div style={{ background: '#fff', padding: 16, minHeight: 1400 }}>
            <h2>用户列表</h2>
            <Table rowKey="id" columns={columns} dataSource={data?.data} pagination={{
                total: data?.total,
                pageSize: pager.size,
                current: pager.page,
                onChange: (page, size) => setPager({page, size}),
                showSizeChanger: true,
                pageSizeOptions: [5, 10, 20],
            }}/>
        </div>
    )
}

export default UserList