import * as React from 'react'
import { api_common, UserInfo } from '../../apis'
import { changeTitle } from '../../store'

const UserDetail:React.FC<{
    params: { id: string }
}> = (props) => {
    const [user, setUser] = React.useState<UserInfo>()

    React.useEffect(function () {
        api_common.getUser(Number(props.params.id)).then(res => {
            setUser(res.data)
            changeTitle(`${res.data.name} 用户详情`)
        })
    }, [props.params.id])

    console.log(props, user)

    return (
        <div style={{ background: '#fff', padding: 16, minHeight: 400 }}>
            <h2>用户详情页</h2>
            <p>姓名: {user?.name} ({['', '男', '女'][user?.gender]})</p>
            <p>年龄: {user?.age}</p>
        </div>
    )
}

export default UserDetail