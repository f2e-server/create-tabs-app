// @ts-check
const PluginPostCSS = require('esbuild-plugin-postcss').default
const plugins = [PluginPostCSS({})]

/**
 * @type { import('f2e-middle-esbuild').BuildOptions }
 */
let config = {
    sourcemap: true,
    entryPoints: ['src/index.tsx'],
    outfile: 'static/bundle.js',
    target: 'chrome70',
    jsxFactory: 'React.createElement',
    bundle: true,
    format: 'iife',
    plugins,
    external: [
        'react',
        'react-dom',
        'react-dom/client',
        'antd',
        '@ant-design/icons',
        'antd/lib/locale/zh_CN',
        'history',
    ],
    loader: {
        '.tsx': 'tsx',
        '.ts': 'ts',
        '.png': 'file',
        '.jpg': 'file',
        '.svg': 'dataurl',
        '.json': 'json',
        '.txt': 'text',
    },
    tsconfig: './tsconfig.json',
};

module.exports = config