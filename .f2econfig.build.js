// @ts-check
 
const options = {
    define: {
        "process.env.NODE_ENV": '"production"',
    },
}
/**
 * @type {import('f2e-server').F2EConfig}
 */
const config = {
    livereload: false,
    build: true,
    useLess: true,
    buildFilter: (pathname) => /^(\.esbuild\/?(external|$)|css|favicon|index|src\/?(index|$)|$)/.test(pathname),
    outputFilter: (pathname) => /^(css|favicon|index|static|$)/.test(pathname),
    middlewares: [
        // 对应文本支持简单ejs语法的服务端渲染，通常用来给资源链接加时间戳
        { middleware: 'template', test: /\.html?/ },
        // 使用 esbuild 编译构建前端资源
        {
            middleware: 'esbuild',
            test: /^\/?static\/bundle\b/,
            watches: [/\.(tsx?|less)$/],
            options,
        },
    ],
    output: require('path').join(__dirname, './output'),
};
module.exports = config;
